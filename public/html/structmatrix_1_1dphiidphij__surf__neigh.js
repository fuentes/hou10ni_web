var structmatrix_1_1dphiidphij__surf__neigh =
[
    [ "coeff", "structmatrix_1_1dphiidphij__surf__neigh.html#a396bea9178a3505a34d298ac99979e18", null ],
    [ "nphi1", "structmatrix_1_1dphiidphij__surf__neigh.html#a5d65f2355ac8ed65151d3fd3a99c6f9c", null ],
    [ "nphi2", "structmatrix_1_1dphiidphij__surf__neigh.html#ae2272f7047a0c4fed3d183c3f3ccd889", null ],
    [ "nsize1", "structmatrix_1_1dphiidphij__surf__neigh.html#a5595d713c39f3bdf5ffcaff59149708f", null ],
    [ "nsize2", "structmatrix_1_1dphiidphij__surf__neigh.html#a38b3003209e324a264900051d5d4d9c6", null ],
    [ "order1", "structmatrix_1_1dphiidphij__surf__neigh.html#a7fa6db286b6c58298244271eb0f1424d", null ],
    [ "order2", "structmatrix_1_1dphiidphij__surf__neigh.html#a7162c29fa259345146d3a26beed844dc", null ]
];