var maphys_8F90 =
[
    [ "edge_border", "maphys_8F90.html#a21343e3d2bdd6a9ba467949f63f30298", null ],
    [ "edge_inner", "maphys_8F90.html#a85f8c07c9086c72916e1a653a2dc3e38", null ],
    [ "edge_physbound", "maphys_8F90.html#ab09d7df16ed4a04e34751d162f43cb27", null ],
    [ "lenindintrf", "maphys_8F90.html#a6ad8c2b5300ea1df8fc3b0681f86f9ec", null ],
    [ "mphs", "maphys_8F90.html#a7002740feea4530a8527164341b92949", null ],
    [ "myindexintrf", "maphys_8F90.html#a9e0621cd095c47302ba65b5a8a83fa26", null ],
    [ "myindexvi", "maphys_8F90.html#a83631336101e3e1ad0d9967a9e8da158", null ],
    [ "myinterface", "maphys_8F90.html#a0408332fa0a4cbbe88d95449b6b7315b", null ],
    [ "mynbvi", "maphys_8F90.html#a613deb7307a01d0b34c0bc588a88edb9", null ],
    [ "myndof", "maphys_8F90.html#aecbb3fb1fdc3a69db11a951190158418", null ],
    [ "myptrindexvi", "maphys_8F90.html#a1c2a7dfa665f4a7c360feb148424fabc", null ],
    [ "mysizeintrf", "maphys_8F90.html#a44dec20d5e4256e9cf278d893fc429d5", null ],
    [ "nedge_border", "maphys_8F90.html#a5e46a9378b36430cfe7797445288019a", null ],
    [ "nedge_inner", "maphys_8F90.html#a3c2e16bf70b24b96aff32406b576ae28", null ],
    [ "nedge_physbound", "maphys_8F90.html#a7657188a51cb8d5de2491fc8c2dd270c", null ],
    [ "tri_physbound", "maphys_8F90.html#a7a2c6ebccff3002c0dcbd88f24351fde", null ]
];