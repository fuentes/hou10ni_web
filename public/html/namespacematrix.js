var namespacematrix =
[
    [ "array2", "structmatrix_1_1array2.html", "structmatrix_1_1array2" ],
    [ "array3_int", "structmatrix_1_1array3__int.html", "structmatrix_1_1array3__int" ],
    [ "dphiidphij", "structmatrix_1_1dphiidphij.html", "structmatrix_1_1dphiidphij" ],
    [ "dphiidphij_surf", "structmatrix_1_1dphiidphij__surf.html", "structmatrix_1_1dphiidphij__surf" ],
    [ "dphiidphij_surf_neigh", "structmatrix_1_1dphiidphij__surf__neigh.html", "structmatrix_1_1dphiidphij__surf__neigh" ],
    [ "mat_phii_ptgl", "structmatrix_1_1mat__phii__ptgl.html", "structmatrix_1_1mat__phii__ptgl" ],
    [ "mat_phii_ptgl_surf", "structmatrix_1_1mat__phii__ptgl__surf.html", "structmatrix_1_1mat__phii__ptgl__surf" ],
    [ "matrix_int", "structmatrix_1_1matrix__int.html", "structmatrix_1_1matrix__int" ],
    [ "matrix_ref", "structmatrix_1_1matrix__ref.html", "structmatrix_1_1matrix__ref" ],
    [ "matrix_ref_surface", "structmatrix_1_1matrix__ref__surface.html", "structmatrix_1_1matrix__ref__surface" ],
    [ "matrix_ref_surface_neigh", "structmatrix_1_1matrix__ref__surface__neigh.html", "structmatrix_1_1matrix__ref__surface__neigh" ],
    [ "matrix_ref_volume", "structmatrix_1_1matrix__ref__volume.html", "structmatrix_1_1matrix__ref__volume" ],
    [ "phii_ptgl", "structmatrix_1_1phii__ptgl.html", "structmatrix_1_1phii__ptgl" ],
    [ "phii_ptgl_surf", "structmatrix_1_1phii__ptgl__surf.html", "structmatrix_1_1phii__ptgl__surf" ],
    [ "value_ref", "structmatrix_1_1value__ref.html", "structmatrix_1_1value__ref" ]
];