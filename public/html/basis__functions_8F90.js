var basis__functions_8F90 =
[
    [ "matrix_polynom_p", "structbasis__functions_1_1matrix__polynom__p.html", "structbasis__functions_1_1matrix__polynom__p" ],
    [ "matrix_polynom_q", "structbasis__functions_1_1matrix__polynom__q.html", "structbasis__functions_1_1matrix__polynom__q" ],
    [ "base", "structbasis__functions_1_1base.html", "structbasis__functions_1_1base" ],
    [ "construct_basis", "basis__functions_8F90.html#a76d7f32ce7425aaa0406426abb44efe5", null ],
    [ "construct_basis_diff", "basis__functions_8F90.html#aa6cf669cc87c1fed8f4d01c8db32debc", null ],
    [ "construct_basis_gamma", "basis__functions_8F90.html#ae7e076797dc5d26234b641075de2d794", null ],
    [ "construct_basis_gamma_neigh", "basis__functions_8F90.html#adc8d3096e99d5f8f22c9fb141da42f87", null ],
    [ "construct_basis_p", "basis__functions_8F90.html#a332e051e3dcea1d19f192e1ffb374ce7", null ],
    [ "construct_basis_q", "basis__functions_8F90.html#ac421f50b843e990656949d78be4247a1", null ]
];