var curved_8F90 =
[
    [ "basis_curved", "curved_8F90.html#ab3170daff62877f8fc0eee1459a81b53", null ],
    [ "basis_curved_1d", "curved_8F90.html#ae644b681cc5d745614d8dadfd6eac091", null ],
    [ "gradphi2d1d_quad_fk", "curved_8F90.html#a3df6340e655dd37b6c990af92094c9f2", null ],
    [ "phi1d_quad_inter", "curved_8F90.html#a899720365bae937085a542eb6e3dfd86", null ],
    [ "phiedge_curved", "curved_8F90.html#ae3fb7adc4a3566c4f351522c4ad64f89", null ],
    [ "value_basis_curved", "curved_8F90.html#acd8917af891d9cf2eabba764bbc29078", null ],
    [ "value_basis_curved_1d", "curved_8F90.html#afd315d6ed6cb8835f1610965971b19c3", null ]
];