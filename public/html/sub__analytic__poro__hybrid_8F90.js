var sub__analytic__poro__hybrid_8F90 =
[
    [ "compute_gradcurl", "sub__analytic__poro__hybrid_8F90.html#a05661aa7c65b1ab5f0d78d7ef7929f3c", null ],
    [ "compute_hessian", "sub__analytic__poro__hybrid_8F90.html#a124619c5fab42d51951ee82196b699e7", null ],
    [ "sub_analytic_poro_hybrid", "sub__analytic__poro__hybrid_8F90.html#a810c5bcc4ecef971db17ec5b83b54f27", null ],
    [ "sub_compute_errors_coeffs", "sub__analytic__poro__hybrid_8F90.html#a43ebed1ce1f7d0fb867bdd59b4f927c1", null ],
    [ "sub_error_poro_p", "sub__analytic__poro__hybrid_8F90.html#ac94c9b20f8f5a86236dd588752a6c811", null ],
    [ "sub_error_w", "sub__analytic__poro__hybrid_8F90.html#a09d9be9ae79b502e858fb22ecc20028b", null ],
    [ "sub_plane_infinite_poro", "sub__analytic__poro__hybrid_8F90.html#a992eeabacd8db821796486278afb1658", null ]
];