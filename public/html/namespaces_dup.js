var namespaces_dup =
[
    [ "analytic", "namespaceanalytic.html", null ],
    [ "basis_functions", "namespacebasis__functions.html", null ],
    [ "bessel_functions", "namespacebessel__functions.html", null ],
    [ "constant", "namespaceconstant.html", null ],
    [ "curved", "namespacecurved.html", null ],
    [ "curved2", "namespacecurved2.html", null ],
    [ "data", "namespacedata.html", null ],
    [ "maphys", "namespacemaphys.html", null ],
    [ "matrix", "namespacematrix.html", null ],
    [ "mesh", "namespacemesh.html", null ],
    [ "mpi_modif", "namespacempi__modif.html", null ],
    [ "mumps", "namespacemumps.html", null ],
    [ "my_mpi", "namespacemy__mpi.html", null ],
    [ "parser", "namespaceparser.html", null ],
    [ "pml", "namespacepml.html", null ],
    [ "polynom", "namespacepolynom.html", null ],
    [ "precision", "namespaceprecision.html", null ],
    [ "solution", "namespacesolution.html", null ],
    [ "tools", "namespacetools.html", null ],
    [ "transient", "namespacetransient.html", null ]
];