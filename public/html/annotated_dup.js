var annotated_dup =
[
    [ "analytic", "namespaceanalytic.html", "namespaceanalytic" ],
    [ "basis_functions", "namespacebasis__functions.html", "namespacebasis__functions" ],
    [ "bessel_functions", "namespacebessel__functions.html", "namespacebessel__functions" ],
    [ "data", "namespacedata.html", "namespacedata" ],
    [ "matrix", "namespacematrix.html", "namespacematrix" ],
    [ "mesh", "namespacemesh.html", "namespacemesh" ],
    [ "parser", "namespaceparser.html", "namespaceparser" ],
    [ "pml", "namespacepml.html", "namespacepml" ],
    [ "polynom", "namespacepolynom.html", "namespacepolynom" ],
    [ "solution", "namespacesolution.html", "namespacesolution" ],
    [ "transient", "namespacetransient.html", "namespacetransient" ]
];