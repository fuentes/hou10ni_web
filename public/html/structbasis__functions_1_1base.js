var structbasis__functions_1_1base =
[
    [ "degree", "structbasis__functions_1_1base.html#a734e4b93ebe0aede1fdf73d2547b6fed", null ],
    [ "der_p", "structbasis__functions_1_1base.html#ac8eb72e0d399fd1c061a4cd0cbdc7c83", null ],
    [ "der_p_gamma", "structbasis__functions_1_1base.html#aed8ab259fdedeed3e7f83f827caf5eb4", null ],
    [ "der_p_gamma_neigh", "structbasis__functions_1_1base.html#a5bfeaeaaa1df2c411de658127ddc52f9", null ],
    [ "der_q", "structbasis__functions_1_1base.html#a2bf1939222ceb183789c354946a1d5a4", null ],
    [ "der_q_gamma", "structbasis__functions_1_1base.html#a3c95bf9f4767aaaec743018f4063562c", null ],
    [ "der_q_gamma_neigh", "structbasis__functions_1_1base.html#aa97d0d93c07fb1fd7c70da96f26227be", null ],
    [ "dim", "structbasis__functions_1_1base.html#a0ad1f98f0ce83735b6b2bea200c00285", null ],
    [ "nphi", "structbasis__functions_1_1base.html#a4ee3ce78b67a3424e29b232f71cfa780", null ],
    [ "p", "structbasis__functions_1_1base.html#afa1c24d9970487f14f250d9d3edf6b92", null ],
    [ "p_gamma", "structbasis__functions_1_1base.html#a811e881cc5a101108043f38313890d53", null ],
    [ "p_gamma_neigh", "structbasis__functions_1_1base.html#a0c95b272a6e3e79442bce3d149dcda8c", null ],
    [ "q", "structbasis__functions_1_1base.html#a3be3151abb43294727546ade492e8dd8", null ],
    [ "q_gamma", "structbasis__functions_1_1base.html#a49734c746f97c9ba0f37623aa1348132", null ],
    [ "q_gamma_neigh", "structbasis__functions_1_1base.html#a9ab7566024c3f000093409970985d2d3", null ],
    [ "type", "structbasis__functions_1_1base.html#a3abee2d8d85cbcad5b9b38f431fc6f30", null ]
];