var searchData=
[
  ['x0_2295',['x0',['../structmesh_1_1cart__data.html#a44c163773c9971fba9e67b0fb133b178',1,'mesh::cart_data::x0()'],['../namespacedata.html#ac8f588f07261e9e01b82c532ceba89d5',1,'data::x0()']]],
  ['x_5fcurved_2296',['x_curved',['../namespacemesh.html#a3d76a2f35960cc1c66a4e3a0db7b44c0',1,'mesh']]],
  ['x_5finit_2297',['x_init',['../namespacedata.html#a0ab17700057a42c4d5502c3559d39e79',1,'data']]],
  ['x_5frcv_2298',['x_rcv',['../namespacedata.html#a4e1b537b0a2e7a0c8df34371182523b1',1,'data']]],
  ['xl_2299',['xl',['../structpml_1_1data__pml.html#aa72bf1f014bccc6fbbadf54e0caa4d1a',1,'pml::data_pml']]],
  ['xr_2300',['xr',['../structpml_1_1data__pml.html#ae97610e934efdb88ca07480d5086024d',1,'pml::data_pml']]]
];
