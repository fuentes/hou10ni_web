var namespacepolynom =
[
    [ "coefficients", "structpolynom_1_1coefficients.html", "structpolynom_1_1coefficients" ],
    [ "operator(*)", "interfacepolynom_1_1operator_07_5_08.html", "interfacepolynom_1_1operator_07_5_08" ],
    [ "polynome_p", "structpolynom_1_1polynome__p.html", "structpolynom_1_1polynome__p" ],
    [ "polynome_q", "structpolynom_1_1polynome__q.html", "structpolynom_1_1polynome__q" ]
];